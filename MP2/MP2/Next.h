//
//  TableViewController.h
//  MP2
//
//  Created by Alex on 12/15/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Next : UITableViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSIndexPath *index;
@property (strong, nonatomic) NSString *item;
@property (strong, nonatomic) NSString *base_Type;
@property (strong, nonatomic) NSArray *types;
@property (strong, nonatomic) NSDictionary *typeDict;
@property double base;
@property (strong, nonatomic) NSIndexPath *lastSelectedRow;
@property (strong, nonatomic) NSString *lastSelectedType;
@property double lastSelectedValue;
//@property NSMutableArray *cells;
//@property (strong, nonatomic) IBOutlet Cell *cellSelected;
- (IBAction)editingChanged:(UITextField *)sender;




@end
