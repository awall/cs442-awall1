//
//  MasterViewController.h
//  MP2
//
//  Created by Alex on 3/29/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreData/CoreData.h>

@interface MasterViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *tableLabel;

@property (strong, nonatomic) NSArray *items;

@property (strong, nonatomic) NSDictionary *categories;

@property (strong, nonatomic) NSDictionary *conversionsDict;

@property (strong, nonatomic) NSArray *arrayCategories;

@end
