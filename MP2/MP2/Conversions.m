//
//  Conversions.m
//  MP2
//
//  Created by Alex on 5/12/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import "Conversions.h"

@implementation Conversions

-(void) getCategories {

    NSString *path = [[NSBundle mainBundle] pathForResource: @"conversions" ofType:@"plist"];

    self.conversionsDict = [[NSDictionary alloc] initWithContentsOfFile: path];

    self.arrayCategories =  self.conversionsDict[@"Categories"];
}

@end
