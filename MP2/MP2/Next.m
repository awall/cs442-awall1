//
//  TableViewController.m
//  MP2
//
//  Created by Alex on 12/15/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import "Next.h"
#import "Cell.h"

@interface Next ()

@end

@implementation Next{
    double converted[20];
    //cells = [[NSMutableArray alloc] init];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.base = 0;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = self.item;
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return [self.types count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];

    static NSString *CellIdentifier = @"nextCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSInteger row = [indexPath row];
    
    NSString *type = self.types[row];
    
    NSArray *base_Array = self.typeDict[self.base_Type];
    NSArray *current_Array = self.typeDict[type];
    
    double m = [current_Array[0] doubleValue];
    double b = [current_Array[1] doubleValue];
    //double converted[self.types.count];
    
    double base_m = [base_Array[0] doubleValue];
    double base_b = [base_Array[1] doubleValue];
    double temp = self.base;
    self.base = (base_m * temp) + base_b;
    
    if (type != self.base_Type) {
        converted[row] = (m * self.base) + b;
    }
    else {
        converted[row] = self.base;
    }
    
    
    UILabel *textLabel = (UILabel *)[cell.contentView viewWithTag:100];
    UITextField *textField = (UITextField *)[cell.contentView viewWithTag:200];
    [textLabel setText:type]; //[self.items objectAtIndex:indexPath.row];
    [textField setText: [NSString stringWithFormat:@"%.5f",converted[row]]];
    // Configure the cell...
    
    //[self.cells addObject:cell];
    
    return cell;
}

- (IBAction)editingChanged:(UITextField *)textField {
    [textField becomeFirstResponder];
    self.lastSelectedValue = [textField.text doubleValue];
    UIView *superview = textField.superview;
    while (![superview isKindOfClass:[UITableViewCell class]]) {
        superview = superview.superview;
    }
    NSInteger row = [self.tableView indexPathForCell:(UITableViewCell *)superview].row;
    
    
    self.lastSelectedType = self.types[row];
    double value = self.lastSelectedValue;
    //    NSArray *base_Array = self.typeDict[self.base_Type];
    NSArray *current_Array = self.typeDict[self.lastSelectedType];
    
    double m = [current_Array[0] doubleValue];
    double b = [current_Array[1] doubleValue];
    
    if (self.lastSelectedType != self.base_Type) {
        self.base = (value/m) - b;
    }
    else {
        self.base = value;
    }
    
    [self.tableView reloadData];

    
}
//- (IBAction)valueChanged:(id)sender {
//    
//    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
//    
//    NSInteger row = [indexPath row];
//    
//    NSString *type = self.types[row];
//    
//    //NSArray *base_Array = self.typeDict[self.base_Type];
//    NSArray *current_Array = self.typeDict[type];
//    
//    double m = [current_Array[0] doubleValue];
//    double b = [current_Array[1] doubleValue];
//    //double converted[self.types.count];
//    
//    //double base_m = [base_Array[0] doubleValue];
//   // double base_b = [base_Array[1] doubleValue];
//    
//    NSString *label = ((UITextField *) sender).text;
//    double value = [label doubleValue];
//    self.base = (value/m) -b;
//    
//    [self.tableView reloadData];
//    
////    double newBase = (value/m) - b;
////    
////    for (int i=0; i<self.types.count; i++) {
////        type = self.types[i];
////        
////        base_Array = self.typeDict[self.base_Type];
////        current_Array = self.typeDict[type];
////        
////        m = [current_Array[0] doubleValue];
////        b = [current_Array[1] doubleValue];
////        converted[i] = (m*newBase) - b;
////    }
//}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
