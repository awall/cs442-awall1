//
//  Conversions.h
//  MP2
//
//  Created by Alex on 5/12/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Conversions : NSObject

@property (strong, nonatomic) NSDictionary *categories;

@property (strong, nonatomic) NSDictionary *conversionsDict;

@property (strong, nonatomic) NSArray *arrayCategories;


- (void) getCategories;

@end
