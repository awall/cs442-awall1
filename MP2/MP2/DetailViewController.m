//
//  DetailViewController.m
//  MP2
//
//  Created by Alex on 3/29/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import "DetailViewController.h"
#import "Cell.h"

@interface DetailViewController ()
//- (void)configureView;
@end

@implementation DetailViewController




#pragma mark - Managing the detail item

//- (void)setDetailItem:(id)newDetailItem
//{
//    if (_detailItem != newDetailItem) {
//        _detailItem = newDetailItem;
//        
//        // Update the view.
//       // [self configureView];
//    }
//}

//- (void)configureView
//{
//    // Update the user interface for the detail item.
//
//    if (self.detailItem) {
//        self.detailDescriptionLabel.text = @"Hello World"; //[[self.detailItem valueForKey:@"timeStamp"] description];
//    }
//}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    //[self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1; //[self.items count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    Cell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[Cell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
   // int row = [indexPath row];
    cell.label.text = @"Hello Alex";//[self.items objectAtIndex:indexPath.row];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView {
    
    
}



@end
