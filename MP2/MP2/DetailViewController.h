//
//  DetailViewController.h
//  MP2
//
//  Created by Alex on 3/29/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource>

//@property (strong, nonatomic) id detailItem;

//@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
