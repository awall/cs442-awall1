//
//  Cell.h
//  MP2
//
//  Created by Alex on 12/15/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Cell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *label;
- (IBAction)valueChanged:(id)sender;


@end
