//
//  ViewController.m
//  Connect4
//
//  Created by Michael Lee on 4/18/14.
//  Copyright (c) 2014 Michael Lee. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
- (void)resetGame;
- (void)newGame: (NSString *) creator;

@end

@implementation ViewController {
    BOOL animating;
    BOOL currentPlayerTurnID;
}

@synthesize pieces;


- (void)awakeFromNib
{
    self.gameModel = [[GameModel alloc] init];
    pieces = [NSMutableArray array];
}

- (void)viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:animated];
    
    self.boardView = [[BoardView alloc] initWithFrame:self.view.bounds slotDiameter:40];
    self.boardView.delegate = self;
    [self.view addSubview:self.boardView];
    [self loadExistingGame];
    
}

- (void)newGame:(NSString *)creator {
    self.navigationItem.hidesBackButton = YES;
    self.Game = [PFObject objectWithClassName:@"Games"];
    self.Game[@"creator"] = creator;
    self.Game[@"opponent"] = @"(No Opponent Yet)";
    self.Game[@"turn"] = @2;
    currentPlayerTurnID = NO;
    self.Game[@"winner"] = @0;
    self.orderedPieces = [[NSMutableArray alloc] init];
    
}

- (void)resetGame
{
    [self.gameModel resetGame];
    
    for (UIView *piece in pieces) {
        [piece removeFromSuperview];
    }
    
    [pieces removeAllObjects];
}

- (void)loadExistingGame {
    for (int i = 0; i < self.orderedPieces.count; i++) {
        NSMutableArray *individualOrderedPieces = [self.orderedPieces objectAtIndex:i];
        int column = [[individualOrderedPieces objectAtIndex:0]intValue];
        if ([self.gameModel processTurnAtCol:column]) {
            
            UIView *circle = [[UIView alloc]
                              initWithFrame:CGRectMake(column*self.boardView.gridWidth-self.boardView.slotDiameter/2.0,
                                                       -self.boardView.slotDiameter,
                                                       self.boardView.slotDiameter,
                                                       self.boardView.slotDiameter)];
            
            circle.layer.cornerRadius = 20;
            self.piece = circle;
            [pieces addObject:self.piece];
            
            self.piece.backgroundColor = [self.gameModel currentColor];
            [self.view insertSubview:self.piece belowSubview:self.boardView];
            
            animating = YES;
            [UIView animateWithDuration:0.001
                             animations:^{
                                 self.piece.center = CGPointMake(column*self.boardView.gridWidth,
                                                                 (6-([self.gameModel topEmptyRowInCol:column]-1))*self.boardView.gridHeight);
                             }
                             completion:^(BOOL finished) {
                                 animating = NO;
                                 if (self.gameModel.gameOver) {
                                     NSLog(@"Player %d won", self.gameModel.winner);
                                     self.Game[@"winner"] = [NSNumber numberWithInt:self.gameModel.winner];
                                     //[self resetGame];
                                 }
                             }];
        }
        [self.view addSubview:self.boardView];
    }
    [self.Game saveInBackground];
}

- (void)boardView:(BoardView *)boardView columnSelected:(int)column
{
    if (animating)
        return;
    if (currentPlayerTurnID)
        return;
    if ([self.gameModel processTurnAtCol:column]) {
        currentPlayerTurnID = YES;
        UIView *circle = [[UIView alloc]
                         initWithFrame:CGRectMake(column*self.boardView.gridWidth-self.boardView.slotDiameter/2.0,
                                                  -self.boardView.slotDiameter,
                                                  self.boardView.slotDiameter,
                                                  self.boardView.slotDiameter)];
        
        circle.layer.cornerRadius = 20;
        self.piece = circle;
        [pieces addObject:self.piece];
        
        NSNumber *turn = [NSNumber numberWithInt: self.gameModel.turn];
        NSMutableArray *individualOrderedPiece = [[NSMutableArray alloc]init];
        [individualOrderedPiece addObject:[NSNumber numberWithInt:column]];
        [individualOrderedPiece addObject:turn];
        [self.orderedPieces addObject:individualOrderedPiece];
        [self.Game setValue:self.orderedPieces forKey:@"orderedPieces"];
        [self.Game setValue:turn forKey:@"turn"];
        [self.Game setValue:self.opponent forKey:@"opponent"];
        
        
        self.piece.backgroundColor = [self.gameModel currentColor];
        [self.view insertSubview:self.piece belowSubview:self.boardView];

        animating = YES;
        [UIView animateWithDuration:0.5
                         animations:^{
                             self.piece.center = CGPointMake(column*self.boardView.gridWidth,
                                                        (6-([self.gameModel topEmptyRowInCol:column]-1))*self.boardView.gridHeight);
                         }
                         completion:^(BOOL finished) {
                             animating = NO;
                             if (self.gameModel.gameOver) {
                                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"WINNER!!!" message:@"You won!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Okay", nil];
                                 [alert show];
                                 NSLog(@"Player %d won", self.gameModel.winner);
                                 self.Game[@"winner"] = [NSNumber numberWithInt:self.gameModel.winner];
                                 [self.Game save];
                                 //[self resetGame];
                             }
                         }];
    }
    
    [self.Game save];
    self.navigationItem.hidesBackButton = NO;
}
@end
