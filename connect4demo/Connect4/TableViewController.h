//
//  MasterViewController.h
//  Connect4
//
//  Created by Alex on 12/18/14.
//  Copyright (c) 2014 Michael Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ParseUI/ParseUI.h>
#import <Parse/Parse.h>

@interface TableViewController : UITableViewController <PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate>
@property (strong, nonatomic) NSMutableArray *_objects;
@property (strong, nonatomic) NSMutableArray *AnewGames;
@property (strong, nonatomic) NSMutableArray *yourTurnGames;
@property (strong, nonatomic) NSMutableArray *waitingTurnGames;
@property (strong, nonatomic) NSMutableArray *finishedGame;
- (IBAction)refreshPull:(id)sender;
- (IBAction)doLogout:(id)sender;

@end
