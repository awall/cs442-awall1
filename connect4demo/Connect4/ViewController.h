//
//  ViewController.h
//  Connect4
//
//  Created by Michael Lee on 4/18/14.
//  Copyright (c) 2014 Michael Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BoardView.h"
#import "GameModel.h"
#import <Parse/Parse.h>

@interface ViewController : UIViewController <BoardViewDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) GameModel *gameModel;
@property (strong, nonatomic) BoardView *boardView;
@property (strong, nonatomic) NSMutableArray *pieces;
@property (strong, nonatomic) UIView *piece;
@property (strong, nonatomic) NSMutableArray *orderedPieces;
@property (strong, nonatomic) PFObject *Game;
@property (strong, nonatomic) NSString *opponent;
- (void)loadExistingGame;
- (void)newGame:(NSString *)creator;
@end
