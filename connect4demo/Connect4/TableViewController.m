//
//  MasterViewController.m
//  Connect4
//
//  Created by Alex on 12/18/14.
//  Copyright (c) 2014 Michael Lee. All rights reserved.
//

#import "TableViewController.h"
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import "ViewController.h"

@interface TableViewController (){
    int newGameCount;
    int yourTurnCount;
    int waitingTurnCount;
    int finishedGameCount;
}
- (void)showLogin;
- (void)loadParseData;
@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadParseData];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidAppear:(BOOL)animated   {
    
    if (![PFUser currentUser]) {
        [self showLogin];
    } else {
        [self performSelector:@selector(loadParseData)];;
    }
    [self loadParseData];
}

- (void)showLogin
{
    PFLogInViewController *logInController = [[PFLogInViewController alloc] init];
    logInController.delegate = self;
    logInController.signUpController.delegate = self;
    [self presentViewController:logInController animated:YES completion:nil];
}

- (IBAction)refreshPull:(id)sender {
    [self performSelector: @selector(loadParseData)];
}

- (IBAction)doLogout:(id)sender {
    [PFUser logOut];
    self._objects = [NSMutableArray array];
    [self showLogin];
}

- (void)loadParseData
{
    PFQuery *Games = [PFQuery queryWithClassName:@"Games"];
    [Games findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            NSLog(@"Parse objects %@", objects);
            self._objects = [objects mutableCopy];
        }
        else{
            NSLog(@"Parse error");
        }
    }];

    self.AnewGames = [[NSMutableArray alloc] init];
    self.finishedGame = [[NSMutableArray alloc] init];
    self.yourTurnGames = [[NSMutableArray alloc] init];
    self.waitingTurnGames = [[NSMutableArray alloc] init];
    
    
    for(int i = 0; i<self._objects.count; i++){
        PFObject *row = [self._objects objectAtIndex: i];
        NSString *creator = [row objectForKey:@"creator"];
        NSString *oponnent = [row objectForKey:@"opponent"];
        int turn = [[row objectForKey:@"turn"] intValue];
        int winner = [[row objectForKey:@"winner"] intValue];

        NSString *currentUsername = [[PFUser currentUser] username];
        
        NSLog(@"%@", currentUsername);
        if ([oponnent isEqualToString:@"(No Opponent Yet)"]) {
            NSLog(@"Game is new and unclaimed");
            if ([currentUsername isEqualToString:creator]) {
                [self.waitingTurnGames addObject:[self._objects objectAtIndex:i]];
            }
            else{
                [self.AnewGames addObject:[self._objects objectAtIndex:i]];
            }
            
        }
        else{
            if (winner != 0) {
                if ([currentUsername isEqualToString:creator]|[currentUsername isEqualToString:oponnent]) {
                    NSLog(@"Game is finished");
                   [self.finishedGame addObject:[self._objects objectAtIndex:i]];
                }
            }
            else{
                if ([creator isEqualToString: currentUsername]) {
                    NSLog(@"currentUser is creator");
                    if (turn == 1) {
                        [self.yourTurnGames addObject:[self._objects objectAtIndex:i]];
                    }
                    else{

                        [self.waitingTurnGames addObject:[self._objects objectAtIndex:i]];
                    }
                    
                }
                else if ([oponnent isEqualToString:currentUsername]){
                    NSLog(@"currentUser is opponent");
                    if (turn == 2) {

                        [self.yourTurnGames addObject:[self._objects objectAtIndex:i]];
                    }
                    else{
                        [self.waitingTurnGames addObject: [self._objects objectAtIndex:i]];
                    }
                }
            }
        }
        
    }
    [self.refreshControl endRefreshing];
    [self.tableView reloadData];
}

- (void)logInViewController:(PFLogInViewController *)controller
               didLogInUser:(PFUser *)user {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self loadParseData];
}

- (void)signUpViewController:(PFSignUpViewController *)signUpController didSignUpUser:(PFUser *)user
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    int numberOfGamesInCurrentSection;
    if (section==0) {
        numberOfGamesInCurrentSection = [self.AnewGames count];
    }
    else if (section==1) {
        numberOfGamesInCurrentSection = [self.yourTurnGames count];
    }
    else if (section==2) {
        numberOfGamesInCurrentSection = [self.waitingTurnGames count];
    }
    else if (section==3) {
        numberOfGamesInCurrentSection = [self.finishedGame count];
    }
    return numberOfGamesInCurrentSection;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *newGameCellIdentifier = @"newGameCell";
    static NSString *yourTurnCellIdentifier = @"yourTurnCell";
    static NSString *waitingTurnCellIdentifier = @"waitingTurnCell";
    static NSString *finishedGameCellIdentifier = @"finishedGameCell";

//    PFObject *row = [self._objects objectAtIndex: index];
//    index++;
//    NSString *creator = [row objectForKey:@"creator"];
//    NSString *oponnent = [row objectForKey:@"opponent"];
//    int turn = [[row objectForKey:@"turn"] intValue];
//    int winner = [[row objectForKey:@"winner"] intValue];
//    NSString *currentUsername = [[PFUser currentUser] username];
    
    
    if (indexPath.section == 0) {
        UITableViewCell *newGameCell = [tableView dequeueReusableCellWithIdentifier:newGameCellIdentifier forIndexPath:indexPath];
        if (newGameCell == nil) {
            newGameCell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:newGameCellIdentifier];
        }
        
        PFObject *temp = [self.AnewGames objectAtIndex:indexPath.row];
        NSString *creator = [temp objectForKey:@"creator"];

        newGameCell.textLabel.text = creator;
        return newGameCell;
    }
    else if (indexPath.section == 1) {
        UITableViewCell *yourTurnCell = [tableView dequeueReusableCellWithIdentifier:yourTurnCellIdentifier forIndexPath:indexPath];
            if (yourTurnCell == nil) {
                yourTurnCell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:yourTurnCellIdentifier];
            }
        UILabel *creatorLabel = (UILabel *)[yourTurnCell.contentView viewWithTag:100];
        UILabel *opponentLabel = (UILabel *)[yourTurnCell.contentView viewWithTag:101];
        PFObject *temp = [self.yourTurnGames objectAtIndex:indexPath.row];
        NSString *creator = [temp objectForKey:@"creator"];
        NSString *opponent = [temp objectForKey:@"opponent"];
        
        [creatorLabel setText: creator];
        [opponentLabel setText: opponent];
        return yourTurnCell;
    }
    else if (indexPath.section == 2) {
        UITableViewCell *waitingTurnCell = [tableView dequeueReusableCellWithIdentifier:waitingTurnCellIdentifier forIndexPath:indexPath];
        if (waitingTurnCell == nil) {
            waitingTurnCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:waitingTurnCellIdentifier];
        }
        UILabel *creatorLabel = (UILabel *)[waitingTurnCell.contentView viewWithTag:200];
        UILabel *opponentLabel = (UILabel *)[waitingTurnCell.contentView viewWithTag:201];
        PFObject *temp = [self.waitingTurnGames objectAtIndex:indexPath.row];
        NSString *creator = [temp objectForKey:@"creator"];
        NSString *opponent = [temp objectForKey:@"opponent"];
        
        [creatorLabel setText: creator];
        [opponentLabel setText: opponent];
        return waitingTurnCell;
    }
    else if (indexPath.section == 3) {
        UITableViewCell *finishedGameCell = [tableView dequeueReusableCellWithIdentifier:finishedGameCellIdentifier  forIndexPath:indexPath];
        if (finishedGameCell == nil) {
            finishedGameCell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:finishedGameCellIdentifier];
        }
        UILabel *person1Label = (UILabel *)[finishedGameCell.contentView viewWithTag:300];
        UILabel *person2Label = (UILabel *)[finishedGameCell.contentView viewWithTag:301];
        PFObject *temp = [self.finishedGame objectAtIndex:indexPath.row];
        NSString *creator = [temp objectForKey:@"creator"];
        NSString *opponent = [temp objectForKey:@"opponent"];
        int winner = [[temp objectForKey:@"winner"] intValue];
        
        if (winner == 1 ) {
            [person1Label setText:creator];
            [person2Label setText:opponent];
        }
        else if(winner == 2){
            [person2Label setText:creator];
            [person1Label setText:opponent];
        }
        else{
            [person2Label setText:@"ERROR"];
            [person1Label setText:@"ERROR"];
        }
        
        return finishedGameCell;
    }
    return nil;
}


-(NSString *)   tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSArray *headers = @[@"Available Games", @"Your turn", @"Their Turn", @"Finished Games"];
    NSString *currentHeader = headers[section];
    
    return currentHeader;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscapeLeft;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"newGame"]) {
        ViewController *vc = [segue destinationViewController];
        [vc newGame: [[PFUser currentUser] username]];
        vc.opponent = @"(No Opponent Yet)";
    }
    if ([[segue identifier] isEqualToString:@"loadExistingGame"]) {
        NSIndexPath *index = [self.tableView indexPathForSelectedRow];

        //For new game without opponent
        if (index.section==0) {
            PFObject *temp = [self.AnewGames objectAtIndex:index.row];
            NSMutableArray *orderedPieces = [temp objectForKey:@"orderedPieces"];
            ViewController *vc = [segue destinationViewController];
            vc.Game = temp;
            vc.orderedPieces = orderedPieces;
            vc.opponent = [[PFUser currentUser] username];
        }
        
        //For game with opponent
        else if (index.section==1) {
            PFObject *temp = [self.yourTurnGames  objectAtIndex:index.row];
            NSString *opp = [temp objectForKey:@"opponent"];
            NSMutableArray *orderedPieces = [temp objectForKey:@"orderedPieces"];
            ViewController *vc = [segue destinationViewController];
            vc.Game = temp;
            vc.orderedPieces = orderedPieces;
            vc.opponent = opp;

        }
        
        //Rest in case of accidental touch
        else if (index.section==2) {
            return;
        }
        else if (index.section==3) {
            return;
        }
    }
}


@end
