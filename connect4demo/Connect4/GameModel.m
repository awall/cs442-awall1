//
//  GameModel.m
//  Connect4
//
//  Created by Alex Wall on 5/1/14.
//  Copyright (c) 2014 Alex Wall. All rights reserved.
//

#import "GameModel.h"

@implementation GameModel {
    int pieces[6][7];       //array keeping track of the players' pieces in the game board
    int _turn;
    int _winner;
    int _numPiecesInCol[8];
}

- (id)init
{
    if (self = [super init]) {
        // initialization
        _turn = 1;

        self.piecesA = [[NSMutableArray alloc] init];
        for (int i = 0; i < 6; i++) {
            NSMutableArray *subArray = [[NSMutableArray alloc] init];
            for (int j = 0; j < 7; j++) {
                [subArray addObject:[NSNumber numberWithInt:0]];
            }
            [self.piecesA addObject:subArray];
        }
    }
    return self;
}

- (void)resetGame
{
    _turn = 1;
    _winner = 0;
    for (int i=0; i<8; i++) {
        _numPiecesInCol[i] = 0;
    }
    for (int i=0; i<6; i++) {
        for (int j = 0; j<7; j++) {
            pieces[i][j] = 0;
        }
    }
    self.piecesA = [[NSMutableArray alloc] init];
    for (int i = 0; i < 8; i++) {
        NSMutableArray *subArray = [[NSMutableArray alloc] init];
        for (int j = 0; j < 9; j++) {
            [subArray addObject:[NSNumber numberWithInt:0]];
        }
        [self.piecesA addObject:subArray];
    }
}

- (int)turn
{
    return _turn;
}

- (UIColor *)currentColor
{
    if (_turn == 1) {
        return [UIColor redColor];
    } else {
        return [UIColor yellowColor];
    }
}

- (BOOL)gameOver
{
    return _winner != 0;
    
}

- (int)winner
{
    return _winner;
}

- (BOOL)processTurnAtCol:(int)col
{
    
    int upRightDiag = 1;    //(/)
    int downRightDiag = 1;  //(\)
    int vert = 1;           //(|)
    int horiz = 1;          //(-)
    
    int row = _numPiecesInCol[col];
    
    if (_numPiecesInCol[col] == 6) {
        return NO;
    }
    
    _numPiecesInCol[col]++;
    
    NSMutableArray *subArray = [self.piecesA objectAtIndex: _numPiecesInCol[col]-1];
    
    [subArray replaceObjectAtIndex:col-1 withObject: [NSNumber numberWithInt:_turn]];
    
    [self.piecesA replaceObjectAtIndex:_numPiecesInCol[col]-1 withObject: subArray];
    
    //pieces[_numPiecesInCol[col]-1][col - 1]= _turn;

    //Here I print the 
    for(int r = 5; r >= 0; r--)
    {
        printf("[ ");
        for(int c = 0; c < 7; c++)
        {
            int num = [self pieceForRow:r col:c];
            pieces[r][c] = num;
            printf("%i ", num);
        }
        printf("]\n");
    }
    printf("\n");
    
    
    //vertical test (|)
    for(int i=row+1; pieces[i][col-1] == _turn && i<=5; i++)
        vert++;
    for(int i=row-1; pieces[i][col-1] == _turn && i>=0; i--)
        vert++;
    if(vert >=  4)
        _winner=_turn;
    
    //horizontal test (-)
    for(int i= col-2; pieces[row][i] == _turn && i>=0; i--) //look left
        horiz++;
    for(int i= col; pieces[row][i] == _turn && i<=6; i++) //look right
        horiz++;

    if(horiz>=4)
        _winner=_turn;
    
    //down right diagonal test (\)
    if (_winner == 0)
    {
        int j = col;

        for(int r=row-1; pieces[r][j]== _turn && r >= 0 && j >= 0; r--) //going down
        {
            j++;
            downRightDiag++;
        }
        j = col+2;

        for(int r=row+1; pieces[r][j]== _turn && r <= 5 && j <= 6; r++) //going up
        {
            j--;
            downRightDiag++;
        }
        if (downRightDiag>=4) {
            _winner = _turn;
        }
    }
    //up right diagonal test (/)
    if (_winner == 0)
    {
        int j = col-2;

        for (int i = row-1; pieces[i][j] == _turn && i>=0 && j <= 6; i--) { //going down

            j--;
            upRightDiag++;

        }
        j = col;

        for (int i = row+1; pieces[i][j] == _turn && i<=5 && j >= 0; i++) { //going up

            i++;
            upRightDiag++;

        }
        if (upRightDiag >= 4) {
            _winner = _turn;
        }
    }
    
    _turn = (_turn == 1) ? 2 : 1;
    return YES;
}

- (int)topEmptyRowInCol:(int)col
{
    return _numPiecesInCol[col];
}

- (int)pieceForRow:(int)row col:(int)col
{
    NSMutableArray *subArray = [self.piecesA objectAtIndex:row];
    return [[subArray objectAtIndex:col] intValue];
}

@end
