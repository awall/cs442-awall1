//
//  GameModel.h
//  Connect4
//
//  Created by Michael Lee on 5/2/14.
//  Copyright (c) 2014 Michael Lee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameModel : NSObject

@property (readonly) int turn;
@property (readonly) UIColor *currentColor;
@property (readonly) BOOL gameOver;
@property (readonly) int winner;
@property (strong, nonatomic) NSMutableArray *piecesA;


- (void)resetGame;
- (BOOL)processTurnAtCol:(int)col;
- (int)topEmptyRowInCol:(int)col;
- (int)pieceForRow:(int)row col:(int)col;

@end
