//
//  ViewController.h
//  Conversion
//
//  Created by Alex on 3/8/14.
//  Copyright (c) 2014 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UITextField *topField;

@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;
@property (weak, nonatomic) IBOutlet UITextField *bottomField;

- (IBAction)convertButtonTapped:(id)sender;

- (IBAction)segmentChanged:(id)sender;

@property (strong) IBOutlet UIButton *arrowButton;

@end
