//
//  ViewController.m
//  Conversion
//
//  Created by Alex on 3/8/14.
//  Copyright (c) 2014 Alex. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

BOOL arrowUp = NO;
int seg = 0;

- (BOOL)canBecomeFirstResponder {
    return YES;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    [super becomeFirstResponder];
    
}
- (IBAction)topPressed:(id)sender {
    arrowUp = NO;
    [self.arrowButton setTitle:@"▼" forState: UIControlStateNormal];
}
- (IBAction)bottomPressed:(id)sender {
    arrowUp = YES;
    [self.arrowButton setTitle:@"▲" forState: UIControlStateNormal];
}


- (IBAction)convertButtonTapped:(id)sender {
    double topValue = 0;
    double bottomValue = 0;
    
    if (arrowUp) {
        if (seg==0) {
            bottomValue = [self.bottomField.text doubleValue];
            bottomValue *= 1.8;
            bottomValue += 32;
            self.topField.text = [NSString stringWithFormat:@"%.5f",bottomValue];
        }
        else if (seg==1) {
            bottomValue = [self.bottomField.text doubleValue];
            bottomValue /= 1.609344;
            self.topField.text = [NSString stringWithFormat:@"%.5f",bottomValue];
        }
        else if (seg==2) {
            bottomValue = [self.bottomField.text doubleValue];
            bottomValue /= 3.785;
            self.topField.text = [NSString stringWithFormat:@"%.5f",bottomValue];
        }
    }
    else {
        if(seg==0){
            topValue = [self.topField.text doubleValue];
            topValue -= 32;
            topValue /= 1.8;
            self.bottomField.text = [NSString stringWithFormat:@"%.5f",topValue];
        }
        else if (seg==1) {
            topValue = [self.topField.text doubleValue];
            topValue *= 1.609344;
            self.bottomField.text = [NSString stringWithFormat:@"%.5f",topValue];
        }
        else if (seg==2) {
            topValue = [self.topField.text doubleValue];
            topValue *= 3.785;
            self.bottomField.text = [NSString stringWithFormat:@"%.5f",topValue];
        }
    }
    [self.topField resignFirstResponder];
    [self.bottomField resignFirstResponder];
}

- (IBAction)segmentChanged:(id)sender {
    seg = (int)((UISegmentedControl *)sender).selectedSegmentIndex;
    NSLog(@"Selected segment is: %d", seg);
    
    if(seg == 0){
        self.topLabel.text = @"ºF";
        self.bottomLabel.text = @"ºC";
    }
    if(seg == 1){
        self.topLabel.text = @"Miles";
        self.bottomLabel.text = @"Kilometers";
    }
    if(seg == 2){
        self.topLabel.text = @"Gallons";
        self.bottomLabel.text = @"Liters";
    }
}

- (IBAction)changeState:(id)sender {
    if (arrowUp) {
        arrowUp = NO;
        [self.arrowButton setTitle:@"▼" forState: UIControlStateNormal];
    }
    else {
        arrowUp = YES;
        [self.arrowButton setTitle:@"▲" forState: UIControlStateNormal];
    }
}
- (IBAction)dismissKeyboard:(id)sender {
    [self.topField resignFirstResponder];
    [self.bottomField resignFirstResponder];
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

@end
